import WatchKit
import Foundation
import SwiftUI

class HostingController : WKHostingController<TopicListView> {
    override var body: TopicListView {
        let model = FlashCardModel()

        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            model.changeAllTheColors()
        }

        return TopicListView(model: model)
    }
}
