import Foundation
import SwiftUI
import Combine

class FlashCardModel: BindableObject {
    var didChange = PassthroughSubject<Void, Never>()
    
    var topics: [TopicDTO] = [
        TopicDTO(title: "Title 1", color: .yellow),
        TopicDTO(title: "Title 2", color: .red),
        TopicDTO(title: "Title 3", color: .blue),
        TopicDTO(title: "Title 4", color: .green),
        TopicDTO(title: "Title 5", color: .orange),
        ] {
        didSet {
            didChange.send(())
        }
    }
    
    func moveTopic(from: IndexSet, to: Int) {
    }
    
    func deleteTopic(at: IndexSet) {
        
    }
    
    func changeAllTheColors() {
        for topic in topics {
            topic.invertColor()
        }
    }
    
}
