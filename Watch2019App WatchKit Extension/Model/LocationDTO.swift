import Foundation
import SwiftUI
import Combine

class LocationDTO: BindableObject {
    var didChange = PassthroughSubject<Void, Never>()
    
    var locationNumber = "12345" {
        didSet {
            didChange.send(())
        }
    }
    
    var streetName = "1234 Main St"  {
        didSet {
            didChange.send(())
        }
    }
    
    var vendorName = "SFMTA"  {
        didSet {
            didChange.send(())
        }
    }
}
