import Foundation
import SwiftUI
import Combine

class TopicDTO: BindableObject {
    var didChange = PassthroughSubject<Void, Never>()
    
    let title: String
    var color: Color
    let tempColor = Color.gray
    var originalColor: Color?
    
    init(title: String, color: Color) {
        self.title = title
        self.color = color
    }
    
    func invertColor() {
        if let oc = self.originalColor {
            self.color = oc
            self.originalColor = nil
        } else {
            self.originalColor = self.color
            self.color = tempColor
        }
    }
}
