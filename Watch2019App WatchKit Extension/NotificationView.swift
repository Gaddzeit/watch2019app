//
//  NotificationView.swift
//  Watch2019App WatchKit Extension
//
//  Created by David Gadd on 2019-06-14.
//  Copyright © 2019 David Gadd. All rights reserved.
//

import SwiftUI

struct NotificationView : View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct NotificationView_Previews : PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
#endif
