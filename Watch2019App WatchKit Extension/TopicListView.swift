 import SwiftUI
 import WatchKit
 
 
 
 struct TopicListView : View {
    @ObjectBinding var model: FlashCardModel
    let height: CGFloat = 100.0

    var body: some View {
        List {
            ForEach(model.topics) { topic in
                NavigationButton(destination: CardList(topic: topic)) {
                    TopicCell(topic: topic)
                        .frame(height: self.height)
                    }
                    .listRowPlatterColor(topic.color)
                }
                .onMove { self.model.moveTopic(from: $0, to: $1) }
                .onDelete { self.model.deleteTopic(at: $0) }
            }
            .listStyle(.carousel)
            .navigationBarTitle(Text("Pop Quiz"))
    }
 }
 
 struct CardList: View {
    var topic: TopicDTO
    
    var body: some View {
        Text(topic.title)
    }
 }
 
 struct TopicCell: View {
    var topic: TopicDTO
    
    var body: some View {
        Text(topic.title)
    }
 }
